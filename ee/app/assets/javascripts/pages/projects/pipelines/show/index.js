import initCCValidationRequiredAlert from 'ee/credit_card_validation_required_alert';
import initPipelineDetails from '~/ci/pipeline_details/pipeline_details_bundle';

initPipelineDetails();
initCCValidationRequiredAlert();
