# frozen_string_literal: true

module EE
  module Search
    module Filter
      extend ::Gitlab::Utils::Override

      private

      override :filters
      def filters
        super.merge(
          language: params[:language],
          label_name: params[:label_name],
          source_branch: params[:source_branch],
          not_source_branch: params[:not_source_branch],
          target_branch: params[:target_branch],
          not_target_branch: params[:not_target_branch],
          author_username: params[:author_username],
          not_author_username: params[:not_author_username],
          fields: params[:fields]
        )
      end
    end
  end
end
