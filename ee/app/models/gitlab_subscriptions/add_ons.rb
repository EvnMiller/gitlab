# frozen_string_literal: true

module GitlabSubscriptions
  module AddOns
    PRODUCT_INTERACTION = {
      code_suggestions: 'duo_pro_add_on_seat_assigned',
      duo_enterprise: 'duo_enterprise_add_on_seat_assigned'
    }.freeze
  end
end
