# frozen_string_literal: true

module CodeSuggestions
  module Tasks
    class Base
      AI_GATEWAY_CONTENT_SIZE = 100_000

      def initialize(params: {}, unsafe_passthrough_params: {}, current_user: nil)
        @feature_setting = ::Ai::FeatureSetting.find_by_feature(feature_setting_name)
        @params = params
        @unsafe_passthrough_params = unsafe_passthrough_params
        @current_user = current_user
      end

      def base_url
        feature_setting&.base_url || Gitlab::AiGateway.url
      end

      def self_hosted?
        feature_setting&.self_hosted?
      end

      def feature_disabled?
        # In case the code suggestions feature is being used via self-hosted models,
        # it can also be disabled completely. In such cases, this check
        # can be used to prevent exposing the feature via UI/API.
        !!feature_setting&.disabled?
      end

      def feature_name
        if self_hosted?
          :self_hosted_models
        else
          :code_suggestions
        end
      end

      def endpoint
        # TODO: After their migration to AIGW, both generations and
        # completions will use the same `/completions` endpoint in v3.
        # See https://gitlab.com/gitlab-org/gitlab/-/issues/477891.
        if endpoint_name == "generations" &&
            !self_hosted? &&
            ::Feature.enabled?(:anthropic_code_gen_aigw_migration, current_user)
          "#{base_url}/v3/code/completions"
        else
          "#{base_url}/v2/code/#{endpoint_name}"
        end
      end

      def body
        body_params = unsafe_passthrough_params.merge(prompt.request_params)

        trim_content_params(body_params)

        body_params.to_json
      end

      private

      attr_reader :params, :unsafe_passthrough_params, :feature_setting, :current_user

      def endpoint_name
        raise NotImplementedError
      end

      # override this method in Tasks::Completion/Generation classes
      def feature_setting_name
        raise NotImplementedError
      end

      def trim_content_params(body_params)
        return unless body_params[:current_file]

        body_params[:current_file][:content_above_cursor] =
          body_params[:current_file][:content_above_cursor].to_s.last(AI_GATEWAY_CONTENT_SIZE)
        body_params[:current_file][:content_below_cursor] =
          body_params[:current_file][:content_below_cursor].to_s.first(AI_GATEWAY_CONTENT_SIZE)
      end
    end
  end
end
